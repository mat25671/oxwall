<?php

class PackageName_BOL_Service {
 

    private static $classInstance;

    public static function getInstance()
    {
        if (self::$classInstance === null)
        {
            self::$classInstance = new self();
        }
    }

    protected function __construct()
    {
        
    }

}